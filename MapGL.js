var MapGL = MapGL || {};

(function() {

    /*
     * EventListener mixin
     */
    let EventListener = (superclass) => class extends superclass {
        on(name, f) {
            if(this._events === undefined)
                this._events = {};
            if(this._events[name] === undefined)
                this._events[name] = [];
            this._events[name].push(f);
        }

        fire(name) {
            if(this._events === undefined)
                this._events = {};
            var stack = this._events[name];
            if(stack !== undefined) {
                var args = Array.prototype.slice.call(arguments, 1);
                for(var i = 0; i < stack.length; i++)
                    stack[i].apply(this, args);
            }
        }
    }

    /*
     * Latitude Longitude location manipulation
     */
    MapGL.LatLng = class LatLng {
        constructor(arg0, arg1) {
            if(arguments.length === 1 && Array.isArray(arg0)) {
                this.lat = arg0[0];
                this.lng = arg0[1];
            }
            else if(arguments.length === 1 && arg0 instanceof Object) {
                if(arg0.lat !== undefined)
                    this.lat = arg0.lat;
                else if(arg0.latitude !== undefined)
                    this.lat = arg0.latitude;
                else
                    throw "Invalid LatLng arguments";

                if(arg0.lng !== undefined)
                    this.lng = arg0.lng;
                else if(arg0.longitude !== undefined)
                    this.lng = arg0.longitude;
                else if(arg0.lon !== undefined)
                    this.lng = arg0.lon;
                else
                    throw "Invalid LatLng arguments";
            }
            else if(arguments.length === 2) {
                this.lat = arg0;
                this.lng = arg1;
            }
            else
                throw "Invalid LatLng arguments";
        }

        get lat() {
            return this._lat;
        }

        get lng() {
            return this._lng;
        }

        set lat(n) {
            if(isNaN(parseFloat(n)) || !isFinite(n))
                throw "Invalid latitude";
            this._lat = n;
        }

        set lng(n) {
            if(isNaN(parseFloat(n)) || !isFinite(n))
                throw "Invalid longitude";
            this._lng = n;
        }

        static create(v) {
            if(v instanceof MapGL.LatLng)
                return v;
            return new MapGL.LatLng(v);
        }

        toString() {
            return '(' + this.lat + ', ' + this.lng + ')';
        }
    };

    MapGL.LatLngBox = class LatLngBox {
        constructor(minLat, minLng, maxLat, maxLng) {
            if(minLat > maxLat || minLng > maxLng)
                throw "Invalid bounds";
            this.bounds = [minLat, minLng, maxLat, maxLng];
        }

        get south() {
            return this.bounds[0];
        }

        get west() {
            return this.bounds[1];
        }

        get north() {
            return this.bounds[2];
        }

        get east() {
            return this.bounds[3];
        }

        extend(v) {
            this.extendPoint(v);
        }

        extendPoint(p) {
            if(p.lat < this.bounds[0] || this.bounds[0] === undefined)
                this.bounds[0] = p.lat;
            if(p.lat > this.bounds[2] || this.bounds[2] === undefined)
                this.bounds[2] = p.lat;
            if(p.lng < this.bounds[1] || this.bounds[1] === undefined)
                this.bounds[1] = p.lng;
            if(p.lng > this.bounds[3] || this.bounds[3] === undefined)
                this.bounds[3] = p.lng;
        }

        containsBox() {
            throw "todo";
        }

        containsPoint(pt) {
            return pt.lat >= this.bounds[0] && pt.lat <= this.bounds[2] && pt.lng >= this.bounds[1] && pt.lat <= this.bounds[3];
        }

        intersects(b) {
            if(b.east < this.west || b.west > this.east || b.north < this.south || b.south > this.north)
                return false;
            return true;
        }

        overlaps(b) {
            if((b.east <= this.west || b.west >= this.east) || (b.north <= this.south || b.south >= this.north))
                return false;
            return true;
        }
    };

    var pi_180 = Math.PI / 180.0;
    var pi_4 = Math.PI * 4;

    MapGL.latToY = function latToY(latitude) {
        var sinLatitude = Math.sin(latitude * pi_180);
        return 0.5 - Math.log((1 + sinLatitude) / (1 - sinLatitude)) / pi_4;
    }

    MapGL.toLat = function toLat(y) {
        return 90 - 360 * Math.atan(Math.exp((y - 0.5) * (2 * Math.PI))) / Math.PI;
    }

    MapGL.loadTexture = function loadTexture(gl, url) {
        // TODO: queue and priority according to center
        return new Promise(function(resolve) {
            var texture = gl.createTexture();
            var image = new Image();
            image.crossOrigin = "anonymous";
            image.onload = function() {
                gl.bindTexture(gl.TEXTURE_2D, texture);
                gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
                gl.generateMipmap(gl.TEXTURE_2D);
                gl.bindTexture(gl.TEXTURE_2D, null);

                resolve(texture);
            };
            image.src = url;
        });
    };

    MapGL.loadProgram = function loadProgram(gl, vsource, fsource) {
        function compileShader(type, source) {
            var shader = gl.createShader(type);
            gl.shaderSource(shader, source);
            gl.compileShader(shader);
            if(!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
                alert(gl.getShaderInfoLog(shader));
                return null;
            }

            return shader;
        }

        var shaderProgram = gl.createProgram();
        gl.attachShader(shaderProgram, compileShader(gl.VERTEX_SHADER, vsource));
        gl.attachShader(shaderProgram, compileShader(gl.FRAGMENT_SHADER, fsource));
        gl.linkProgram(shaderProgram);
        if(!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
            alert("Could not initialise shaders");
        }
        return shaderProgram;
    };

    MapGL.Map = class Test extends EventListener(Object) {
        constructor(element) {
            super();

            var self = this;
            this._element = element;
            var canvas = this._canvas = document.createElement('canvas');
            this._dirty = false;
            this._layers = [];
            var width = this._width = element.offsetWidth;
            var height = this._height = element.offsetHeight;
            canvas.width = width;
            canvas.height = height;
            this._targetZ = this._z = 0; //Math.ceil(Math.log(Math.min(width, height) / 256) / Math.log(2));
            this.location = new MapGL.LatLng([0, 0]);
            this._element.appendChild(canvas);
            var gl;
            try {
                this._gl = gl = canvas.getContext("experimental-webgl");
                gl.clearColor(0.0, 0.0, 0.0, 0.0);
                gl.clearDepth(1.0);
                gl.enable(gl.DEPTH_TEST);
                gl.depthFunc(gl.LEQUAL);
            }
            catch(e) {
            }
            if(!gl) {
                alert("Could not initialise WebGL, sorry :-( ");
            }

            (function() {
                canvas.addEventListener('click', self.onClick.bind(self), false);
            })();

            (function() {
                function onMouseWheel(e) {
                    var dir = 0;
                    if('wheelDelta' in e)
                         dir = Math.sign(e.wheelDelta);
                     else
                         dir = Math.sign(-e.detail);

                    self.setZoom(self._targetZ + dir, true, self.unproject([e.offsetX, e.offsetY]));
                    self.dirty();
                }
                canvas.addEventListener("mousewheel", onMouseWheel, false);
                canvas.addEventListener("DOMMouseScroll", onMouseWheel, false);
            })();

            (function() {
                canvas.addEventListener("mousedown", function(e) {
                    canvas.style.cursor = '-webkit-grabbing';
                    this._startX = e.offsetX;
                    this._startY = e.offsetY;
                }.bind(this), false);

                canvas.addEventListener("mousemove", function(e) {
                    if(this._startX !== undefined) {
                        var dx = e.offsetX - this._startX,
                            dy = e.offsetY - this._startY;

                        var worldSize = Math.floor(256 * Math.pow(2, this._z));
                        this._x -= dx / worldSize;
                        this._y -= dy / worldSize;

                        this._startX = e.offsetX;
                        this._startY = e.offsetY;

                        this.dirty();
                        this.fire('viewchange');
                    }
                }.bind(this), false);

                canvas.style.cursor = '-webkit-grab';
                canvas.addEventListener("mouseup", function(e) {
                    this._startX = this._startY = undefined;
                    canvas.style.cursor = '-webkit-grab';
                }.bind(this), false);

            }.bind(this))();

            this._lastTime = new Date().getTime() / 1000;
            this.dirty();
        }

        addLayer(layer, lat, lng) {
            this._layers.push(layer);
            layer._map = this;
            this.dirty();
        }

        get location() {
            return this._location;
        }

        set location(loc) {
            this._location = MapGL.LatLng.create(loc);
            this._x = this.location.lng / 360;
            this._y = MapGL.latToY(this.location.lat) - 0.5;
            this.fire('viewchange');
        }

        zoomIn(animated, center) {
            this.setZoom(this._targetZ + 1, animated === undefined ? true : animated, center);
        }

        zoomOut(animated, center) {
            this.setZoom(this._targetZ - 1, animated === undefined ? true : animated, center);
        };

        setZoom(z, animated, center) {
            if(z < 0)
                z = 0;
            if(animated) {
                this._targetZ = Math.floor(z);
                this._zoomStartZ = this._z;
                this._zoomStart = new Date().getTime() / 1000;
                this._zoomStartX = this._x;
                this._targetX = center ? center[1] / 360 : this._x;
                this._zoomStartY = this._y;
                this._targetY = center ? MapGL.latToY(center[0]) - 0.5 : this._y;
            }
            else {
                this._targetZ = this._z = Math.floor(z);
            }
            this.dirty();
            this.fire('viewchange');
        }

        fitBounds(bounds) {
            var lat = bounds.minLat / 2 + bounds.maxLat / 2;
            var lng = bounds.minLng / 2 + bounds.maxLng / 2;
            this.location = [lat, lng];
            var minY = MapGL.latToY(bounds.minLat),
                maxY = MapGL.latToY(bounds.maxLat);

            var a = 256 * ((bounds.maxLng - bounds.minLng) / 360);
            var b = 256 * (minY - maxY);
            for(var z = 0; z < 20; z++) {
                var viewWidth = a * Math.pow(2, z);
                var viewHeight = b * Math.pow(2, z);
                if(viewWidth > this._width || viewHeight > this._height) {
                    this.setZoom(z - 1);
                    this.fire('viewchange');
                    break;
                }
            }
        }

        onClick(event) {
            for(var i = 0; i < this._layers.length; i++) {
                var layer = this._layers[i];
                if('onClick' in layer) {
                    if(!layer.onClick(event))
                        break;
                }
            }
        }

        get bounds() {
            var northWest = this.unproject([0, 0]);
            var southEast = this.unproject([this._width, this._height]);

            return new MapGL.LatLngBox(southEast[0], northWest[1], northWest[0], southEast[1]);
        }

        project(loc) {
            var worldSize = 256 * Math.pow(2, this._z);

            return [
                worldSize * (loc.lng / 360 - this._x) + this._width / 2,
                worldSize * (MapGL.latToY(loc.lat) - this._y - 0.5) + this._height / 2
            ];
        }

        unproject(pos) {
            var worldSize = 256 * Math.pow(2, this._z);

            return [
                MapGL.toLat((pos[1] - this._height / 2) / worldSize + this._y + 0.5),
                360 * ((pos[0] - this._width / 2) / worldSize + this._x)
            ];
        }

        update() {
            if(this._width !== this._element.offsetWidth || this._height !== this._element.offsetHeight) {
                this._canvas.width = this._width = this._element.offsetWidth;
                this._canvas.height = this._height = this._element.offsetHeight;
            }
            this._dirty = false;
            var time = new Date().getTime() / 1000;

            var animated = false;
            if(this._zoomStart !== undefined) {
                var t = (time - this._zoomStart) * 4;
                if(t >= 1) {
                    this._z = this._targetZ;
                }
                else {
                    this._z = this._targetZ * t + this._zoomStartZ * (1 - t);
                    this._x = this._targetX * t + this._zoomStartX * (1 - t);
                    this._y = this._targetY * t + this._zoomStartY * (1 - t);
                    this.fire('viewchange');
                    animated = true;
                }
            }

            if(Math.abs(this._y) > 0.5)
                this._y = 0.5 * Math.sign(this._y);

            this._location.lng = this._x * 180;
            this._location.lat = MapGL.toLat(0.5 + this._y);

            this.render(animated);
            if(animated)
                this.dirty();
        }

        render(animation) {
            var gl = this._gl;
            gl.viewport(0, 0, this._width, this._height);
            gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

            var numTiles = Math.pow(2, this._z);
            var proj = mat4.ortho(mat4.create(), 0, this._width, this._height, 0, -100, 1000);

            this._layers.forEach(function(layer) {
                layer.render(this, gl, proj, this._z, animation);
            }.bind(this));
        }

        dirty() {
            if(this._width !== this._element.offsetWidth || this._height !== this._element.offsetHeight) {
                this._canvas.width = this._width = this._element.offsetWidth;
                this._canvas.height = this._height = this._element.offsetHeight;
            }
            if(this._dirty === false)
                window.requestAnimationFrame(this.update.bind(this));
            this._dirty = true;
        }
    };



    /*
     * Tile
     */
    var Tile = function(map, layer, z, x, y) {
        this._children = [null, null, null, null];
        this._map = map;
        this._z = z;
        this._x = x;
        this._y = y;
        this._loading = false;
        this._layer = layer;

        var self = this;
        // calculs et uniforms en entier bordel
    };

    Tile.prototype.isReady = function() {
        return this._ready;
    };

    Tile.prototype.areChildrenReady = function() {
        var now = new Date().getTime();
        for(var i = 0; i < 4; i++) {
            var child = this._children[i];
            if(!child || !child.isReady())
                return false;
            if(child._blendInAnimation !== undefined)
                return false;
        }
        return true;
    };

    Tile.prototype.getScreenPosition = function(animation) {
        var worldSize = 256 * Math.pow(2, this._map._z);
        var size = 256 * Math.pow(2, this._map._z - this._z);
        var x = (this._x * size + this._map._width / 2 - size * 0.5 * Math.pow(2, this._z) - worldSize * this._map._x),
            y = (this._y * size + this._map._height / 2 - size * 0.5 * Math.pow(2, this._z) + worldSize * -this._map._y);
        return {
            x: animation ? x : Math.floor(x),
            y: animation ? y : Math.floor(y),
            size: size,
        };
    };

    Tile.prototype.render = function(gl, z, animation) {
        var screenPosition = this.getScreenPosition(animation);
        if(screenPosition.x > this._map._width || screenPosition.y > this._map._height)
            return;
        if(screenPosition.x + screenPosition.size < 0 || screenPosition.y + screenPosition.size < 0)
            return;

        var render = function() {
            if(this._texture) {
                var alpha = 1.0;
                if(this._blendInAnimation !== undefined) {
                    var d = new Date().getTime() - this._blendInAnimation;
                    alpha = d / 1000;
                }

                gl.bindTexture(gl.TEXTURE_2D, this._texture);
                /*if(this._x === 1 && this._y === 1)
                    gl.uniform3f(gl.getUniformLocation(MapGL.TileLayer.program, "uPos"), screenPosition.x + 128, screenPosition.y + 128, this._z / 100);
                else*/
                gl.uniform3f(gl.getUniformLocation(MapGL.TileLayer.program, "uPos"), screenPosition.x, screenPosition.y, this._z / 100);
                gl.uniform1f(gl.getUniformLocation(MapGL.TileLayer.program, "uSize"), screenPosition.size);
                gl.uniform1f(gl.getUniformLocation(MapGL.TileLayer.program, "uAlpha"), alpha);
                gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);

                if(this._blendInAnimation !== undefined) {
                    gl.disable(gl.BLEND);
                    this._map.dirty();
                    if(d > 1000)
                        this._blendInAnimation = undefined;
                }
            }
        }.bind(this);

        if(z > this._z) {
            if(!this._children[0]) {
                this._children[0] = new Tile(this._map, this._layer, this._z + 1, this._x * 2 + 0, this._y * 2 + 0);
                this._children[1] = new Tile(this._map, this._layer, this._z + 1, this._x * 2 + 1, this._y * 2 + 0);
                this._children[2] = new Tile(this._map, this._layer, this._z + 1, this._x * 2 + 0, this._y * 2 + 1);
                this._children[3] = new Tile(this._map, this._layer, this._z + 1, this._x * 2 + 1, this._y * 2 + 1);
            }
            if(!this.areChildrenReady()) {
                render();
            }
            for(var i = 0; i < 4; i++) {
                var child = this._children[i];
                if(child)
                    child.render(gl, z, animation);
            }
        }
        else if(!this._texture && this._loading === false) {
            this._loading = true;
            var self = this;
            MapGL.loadTexture(gl, this._layer.getTileURL(this._z, this._x, this._y)).then(function(texture) {
                self._texture = texture;
                self._map.dirty();
                self._loading = false;
                self._ready = true;
                self._blendInAnimation = new Date().getTime();
            });
        }
        else {
            render();
        }

    };



    MapGL.Layer = class Layer {
        constructor() {
        }

        render() {
        }
    };

    /*
     * TileLayer
     */
    MapGL.TileLayer = function(params) {
        this._params = params;
        this._maxZ = params.maxZ;
        this._urls = [
            'https://mt0.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',
            'https://mt1.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',
            'https://mt2.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',
            'https://mt3.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',
        ];
    };

    MapGL.TileLayer.prototype.getTileURL = function(z, x, y) {
        var url = this._urls[Math.floor(Math.random() * this._urls.length)];
        url = url.replace('{x}', x);
        url = url.replace('{y}', y);
        url = url.replace('{z}', z);
        return url;
    };

    MapGL.TileLayer.prototype.render = function(map, gl, proj, z, animation) {
        if(this._root === undefined) {
            this._map = map;

            MapGL.TileLayer.program = MapGL.loadProgram(gl, [
                "precision lowp float;",
                "attribute vec2 aPos;",
                "varying vec2 vTexCoord;",
                "uniform mat4 uProj;",
                "uniform vec3 uPos;",
                "uniform float uSize;",

                "void main() {",
                "   gl_Position = uProj * vec4(vec3(aPos.xy * uSize, 0.0) + uPos, 1.0);",
                "   vTexCoord = aPos;",
                "}",
            ].join('\n'), [
                "precision lowp float;",
                "varying vec2 vTexCoord;",
                "uniform sampler2D uSampler;",
                "uniform float uAlpha;",

                "void main() {",
                "   gl_FragColor = /*uAlpha * */texture2D(uSampler, vTexCoord);",
                //"   gl_FragColor = vTexCoord.x < uAlpha ? texture2D(uSampler, vTexCoord) : vec4(1.0, 0.0, 0.0, 0.0);",
                "}",
            ].join('\n'));

            MapGL.TileLayer.quad = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, MapGL.TileLayer.quad);
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([
                0, 0,
                1, 0,
                0, 1,
                1, 1]), gl.STATIC_DRAW);

            this._root = new Tile(map, this, 0, 0, 0);
        }

        gl.useProgram(MapGL.TileLayer.program);
        var loc = gl.getAttribLocation(MapGL.TileLayer.program, "aPos");
        gl.bindBuffer(gl.ARRAY_BUFFER, MapGL.TileLayer.quad);
        gl.enableVertexAttribArray(loc);
        gl.vertexAttribPointer(loc, 2, gl.FLOAT, false, 0, 0);

        gl.activeTexture(gl.TEXTURE0);
        gl.uniform1i(gl.getUniformLocation(MapGL.TileLayer.program, "uSampler"), 0);
        gl.uniformMatrix4fv(gl.getUniformLocation(MapGL.TileLayer.program, "uProj"), false, proj);

        //gl.enable(gl.BLEND);
        //gl.blendFunc(gl.ONE, gl.ZERO);
        this._root.render(gl, z, animation);
        //gl.disable(gl.BLEND);
    };

    MapGL.PointsLayer = function() {
        this._data = [
            43.6098829, 3.8748718,
        ];
    };

    MapGL.PointsLayer.prototype.render = function(map, gl, proj, z, animation) {
        if(this._buffer === undefined) {
            this._map = map;

            MapGL.PointsLayer.program = MapGL.loadProgram(gl, [
                "precision highp float;",
                "attribute vec2 aPos;",
                "uniform mat4 uProj, uView;",

                "void main() {",
                "   gl_Position = uProj * uView * vec4(aPos, 1.0, 1.0);",
                "   gl_PointSize = 10.0;",
                "}",
            ].join('\n'), [
                "precision highp float;",

                "void main() {",
                "   gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);",
                "}",
            ].join('\n'));

            this._buffer = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, this._buffer);
            var rawData = new Float32Array(this._data);
            for(var i = 0; i < this._data.length; i += 2) {
                rawData[i] = this._data[i + 1] / 360;
                rawData[i + 1] = MapGL.latToY(this._data[i]) - 0.5;
            }
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(rawData), gl.STATIC_DRAW);
        }

        gl.useProgram(MapGL.PointsLayer.program);
        var loc = gl.getAttribLocation(MapGL.PointsLayer.program, "aPos");
        gl.bindBuffer(gl.ARRAY_BUFFER, this._buffer);
        gl.enableVertexAttribArray(loc);
        gl.vertexAttribPointer(loc, 2, gl.FLOAT, false, 0, 0);

        gl.uniformMatrix4fv(gl.getUniformLocation(MapGL.PointsLayer.program, "uProj"), false, proj);


        var worldSize = Math.floor(256 * Math.pow(2, this._map._z));

        var view = mat4.create();
        mat4.translate(view, view, [this._map._width / 2, this._map._height / 2, 0]);
        mat4.scale(view, view, [worldSize, worldSize, 1]);
        mat4.translate(view, view, [-this._map._x, -this._map._y, 0]);
        gl.uniformMatrix4fv(gl.getUniformLocation(MapGL.PointsLayer.program, "uView"), false, view);

        gl.drawArrays(gl.POINTS, 0, this._data.length / 2);
    };

})();
